import tensorflow as tf

with open("/Users/marcouras/PycharmProjects/tensorface/vgg/tf/vggface16.tfmodel", mode='rb') as f:
    fileContent = f.read()


graph_def = tf.GraphDef()
graph_def.ParseFromString(fileContent)
tf.import_graph_def(graph_def)
graph = tf.get_default_graph()


tf.import_graph_def(graph_def)
#saver = tf.train.Saver()

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())

    tf.train.write_graph(sess.graph_def, ".", 'vggtensor.pbtxt')
    #saver.save(sess,'vggtensor.ckpt')
    print("merda")

