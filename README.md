# FACE RECOGNITION AND ADVERSARIAL EXAMPLES WITH TENSORFLOW #
Passi necessari per il funzionamento del software associato alla tesina di Biometria ed Integrazione del corso di Machine Learning.

- [FACE RECOGNITION AND ADVERSARIAL EXAMPLES WITH TENSORFLOW](#markdown-header-face-recognition-and-adversarial-examples-with-tensorflow)
  * [Requisiti](#markdown-header-requisiti)
  * [Introduzione a tensorflow](#markdown-header-introduzione-a-tensorflow)
    + [Grafi computazionali](#markdown-header-grafi-computazionali)
    + [Definizione del grafo: nodi e tipi di dati](#markdown-header-definizione-del-grafo)
        - [Costanti](#markdown-header-costanti)
        - [Placeholder](#markdown-header-placeholder)
        - [Variabili](#markdown-header-variabili)
        - [Inizializzazione](#markdown-header-inizializzazione)
    + [Sessione](#markdown-header-sessione)
    + [Run](#markdown-header-run)
  * [Tensorboard: Teoria](#markdown-header-introduzione-a-tensorflow)
    + [Visualizzazione del grafico di calcolo](#markdown-header-visualizzazione-del-grafico-di-calcolo)
    + [Visualizzazione dei dati di sintesi](#markdown-header-visualizzazione-dei-dati-di-sintesi)  
  * [Contenuto Repository](#markdown-header-contenuto-repository)
  * [Setup Esperimento](#markdown-header-setup-esperimento)
    + [Classificazione](#markdown-header-classificazione)
    + [Adversarial Example](#markdown-header-adversarial-example)
      - [Implementazione di "find adversary noise"](#markdown-header-implementazione-di--find-adversary-noise-)
    + [Tensorboard: Esempio](#markdown-header-tensorboard)
  * [Contatti](#markdown-header-contatti)

## Requisiti ##

* Tensorflow
* Tensorboard
* Requirements.txt

## Introduzione a tensorflow ##
TensorFlow è una libreria ideata per la computazione numerica attraverso grafi computazionali ("Flow Graph").

###Grafi Computazionali (Flow Graph)

I grafi computazionali sono delle sequenze di nodi connessi, dove ogni nodo è una specifica operazione (chiamata ops) che prende in input zero o più tensori e restituisce zero o più tensori. 
Un’applicazione TensorFlow, in generale è composta dalle seguenti sezioni:

* Definire il grafo: si definisce come funziona ogni nodo e ogni input
* Creare la sessione → sess = tf.session()
* [Inizializzare](#markdown-header-inizializzazione) la sessione → sess.run(init)
* Avviare la sessione: si esegue il flusso completo del grafo → sess.run(var)
* Chiudere la sessione → sess.close() 

Gli archi in un grafo sono array multidimensionali e sono chiamati Tensori. Ogni Nodo in TensorFlow, accetta in input dei Tensori, gli applica delle funzioni matematiche e restituisce in output al nodo successivo il Tensore risultante.

###Definizione del grafico
In TensorFlow è necessario definire i valori di input del grafico. Esistono tre tipi di input: 

* [Costanti](#markdown-header-costanti)
* [Placeholders](#markdown-header-placeholders)
* [Variabili](#markdown-header-variabili)


####Costanti
Le costanti sono pensate per mantenere lo stesso valore ad ogni esecuzione del grafo. 

Esempio: 
```python
x = tf.constant(1.3)    # Float
y = tf.constant(2) 	     # Intero
```
####Placeholders
Se vogliamo poter definire i valori in input utilizzati nel flow graph in runtime, ovvero ad ogni esecuzione del grafo vengono assegnati dei valori diversi, dobbiamo creare dei placeholder come segue:
```python
x = tf.placeholder(tf.float32)
y = tf.placeholder(tf.float32)
```

Quando creiamo un placeholder, non dobbiamo definire il suo valore, in quanto esso va definito in fase di computazione (e non in fase di definizione del grafo). Dobbiamo comunque definire il tipo di dato del placeholder (di tipo float nell’esempio).
Durante la fase di run i valori del placeholder verranno caricati attraverso un dizionario denominato “feed_dict”.

Esempio:
```python
sess.run(result,feed_dict={a:1,b:5,c:2,d:5,e:6})
```
####Variabili
Quando  si effettua l’addestramento di un modello si ha la necessità di strutture che permettano di mantenere e aggiornare dei parametri. Per ottenere questo livello di persistenza, si utilizzano le variabili. 

Definiamo una variabile come segue:
```python
var = tf.Variable(0)
```

####Inizializzazione
Quando creiamo una variabile prima di poterci accedere, è necessario specificare come inizializzarla. In questo esempio quando la variabile verrà inizializzata avrà valore zero. 
Per inizializzare le variabili possiamo creare un nodo "inizializzatore" che inizializza tutte le variabili, la sintassi è la seguente:
```python
init = tf.initialize_all_variables() 
sess = tf.Session()
sess.run(init)
```
In generale possiamo dire che quando si Inizializza la sessione, i diversi oggetti di tipo "tf" che abbiamo dichiarato nel codice vengono allocati in memoria. 

###Sessione

Una volta definito il flow graph, per calcolare il risultato dobbiamo prima lanciare una sessione del grafo corrente:
```python
sess = tf.Session()
```
###Run
Una volta definito il grafo e avviata la sessione rimane da lanciare la computazione chiedendo a tensorflow di restituire il valore di un determinato tensore "output":
```python
sess.run(output)
```

Eseguendo quest'istruzione, che prende un insieme di nomi di output che richiedono di essere calcolati, 
TensorFlow può calcolare la chiusura transitiva di tutti i nodi che devono essere eseguiti in ordine per calcolare
gli output richiesti e può provvedere ad eseguire i nodi appropriati in un ordine
che rispetta le loro dipendenze, non viene quindi eseguito necessariamente tutto il flusso ma solo la parte necessaria alla computazione degli output richiesti.

È inoltre possibile spezzare la computazione e dare un nome ai tensori intermedi:
```python
midtens1 = a+b+c

midtens2 = x*d

output = midtens2/6

print sess.run(midtens1)

print sess.run(midtens2)

print sess.run(output)
```
Quando abbiamo finito con il grafo, possiamo chiudere la sessione. Ciò distruggerà il contenuto dei tensori che abbiamo definito.
```
sess.close()
```


##TensorBoard

Al fine di aiutare gli utenti a comprendere la struttura dei loro grafici di calcolo e anche per capire il comportamento generale di modelli di apprendimento automatico, è stato costruito TensorBoard, uno strumento di visualizzazione delle strutture del grafico e delle statistiche riassuntive per TensorFlow.

###Visualizzazione del grafico di calcolo

Molti dei grafici di calcolo delle Deep Neural Network possono essere molto complessi.
Ad esempio, il grafico di calcolo per il training di un modello simile al modello Inception di Google, una profonda rete neurale convoluzionale che ha avuto “the best classification performance” nel contest ImageNet 2014, ha più di 36.000 nodi nel suo grafico di calcolo TensorFlow. A causa delle dimensioni e della topologia di questi grafici le ingenue tecniche di visualizzazione producono diagrammi disordinati. Per aiutare gli utenti a vedere la sottostante organizzazione dei grafici, gli algoritmi in TensorBoard collassano i nodi in blocchi di alto livello, mettendo in evidenza gruppi con strutture identiche. Il sistema separa anche i nodi di alto grado, che spesso svolgono funzioni di compatibilità in una zona separata dello schermo. In questo modo si riduce l’ingombro visivo e si focalizza l’attenzione sulle sezioni principali del grafico di calcolo. L’intera visualizzazione è interattiva: gli utenti
possono fare una panoramica, zoomare e espandere i nodi raggruppati scavando a fondo per i dettagli. 

###Visualizzazione dei dati di sintesi
Quando si formano modelli di apprendimento automatico, gli utenti spesso vogliono essere in grado di esaminare lo stato dei vari aspetti del modello, e come questo i vari cambiamenti di stato nel corso del tempo. A tal fine, TensorFlow supporta una raccolta di diverse operazioni di sintesi che possono essere inserite nel grafico, compresi riassunti scalari (ad esempio, per esaminare le proprietà generali del modello, come il valore della funzione loss mediati attraverso una raccolta di esempi, o il tempo necessario per eseguire il grafico di calcolo), riassunti basati su istogrammi (ad esempio, la distribuzione dei valori di peso in un livello di rete neurale), o riassunti basati su immagini (ad esempio, una visualizzazione dei pesi dei filtri appresi in una rete neurale convoluzionale). Tipicamente i grafici di calcolo sono impostati in modo che i nodi di sintesi vengano inclusi per monitorare vari valori interessanti, e ogni tanto durante l’esecuzione del grafico di training, l’insieme dei nodi di sintesi vengono
eseguiti, in aggiunta alla normale serie di nodi che vengono eseguiti, e il programma driver del client scrive i dati di sintesi in un file log associato con il training del modello. Il programma TensorBoard è quindi configurato per vedere questo file log per i nuovi record di sintesi, e in grado di visualizzare le informazioni di riepilogo e come cambia nel tempo (con la possibilità di selezionare la misura del “tempo” per essere relativo al “wall time” dall’inizio dell’esecuzione del programma TensorFlow, tempo assoluto, o “steps”, una misura numerica del numero di esecuzioni del grafico che si sono verificate dopo l’inizio dell’esecuzione del programma TensorFlow).







## Contenuto Repository ##
Sono inclusi sostanzialmente 2 script, un modello di classificazione già addestrato ed un riferiento testuale alle classi di cui è
composto il dataset.

* *Clssify.py*
è lo script che effettua la classificazione, il flusso seguito in questa classe parte da una funziona che effettua il download di un'immagine
prendendo in ingresso un link, carica il modello che verrà poi utilizzato per etichettare l'immagine scaricata.

* *Adv.py*
questo script crea un adversarial example, aggiungengo in maniera iterativa del particolare rumore fino ad imbrogliare il classificatore
indipendentemente dal fatto che l'immagine appartenga ad una classe presente nel dataset o meno.

* *vgg_graph.py*
dove vengono definiti dei metodi di utilità per utilizzare il grafo.

* Il *[modello vggface](https://github.com/pavelgonchar/vgg-face-tensorflow)*
è quello utilizzato in questo esperimento, convertito dal modello già pronto per matlab, caffe e torch presente a questo [link](http://www.robots.ox.ac.uk/~vgg/software/vgg_face/)
sviluppati all'Università di Oxford.

## Setup Esperimento ##


### Classificazione ###
Si parte con la classificazione dell'immagine, come detto in precedenza questa può essere scaricata semplicemente fornendo un link alla funzione seguente
```python
image_url = "http://7wallpapers.net/wp-content/uploads/7_Adam-Goldberg.jpg"
download_image(image_url)
```
Viene poi ridimensionata.
```python
image = skimage.io.imread('./vgg/tf/target.jpg') / 255.0
image = resize(image, (224, 224))
image = image.reshape((1, 224, 224, 3))

```

A questo punto deve essere caricato il grafo a partire da un modello già addestrato, per comodità abbiamo caricato il modello all'interno di questo
repository esso si trova all'interno del path vgg/tf/vggface16.tfmodel. Dunque scaricata l'immagine e noto il modello non ci resta altro che definire
una variabile all'interno del grafo caricato usando uno dei metodi forniti dal framework Tensorflow.
Le istruzione seguenti definiscono la variabile all'interno del grafo, e caricano la sua struttura a partire da modello scaricato.
```python
content = tf.Variable(image, dtype=tf.float32)
with open("vgg/tf/vggface16.tfmodel", mode='rb') as f:
  	fileContent = f.read()
graph_def = tf.GraphDef()
graph_def.ParseFromString(fileContent)
tf.import_graph_def(graph_def, input_map={"images": content})
graph = tf.get_default_graph()
```
Definiamo creiamo poi un file di log utile che utilizzeremo in seguito con [Tensorboard](https://www.tensorflow.org/get_started/summaries_and_tensorboard)
```python
log_path = "./logs"
writer = tf.summary.FileWriter(logdir=log_path, graph=tf.get_default_graph())
```
dobbiamo ricordarci di chiudere il writer una volta chiusa la sessione di tensorflow.

Adesso carichiamo dal grafo i tensori che vogliamo calcolare e avviamo una sessione. Ricordiamo che affinchè possa essere calcolato il valore di uno
dei tensori che sta nelle parti finali del grafo, ad esempio nell'ultimo layer, tensorflow deve calcolare il valore di tutti i tensori precedenti.
Di seguito importiamo il tensore responsabile per il calcolo delle probabilità di classe che nel nostro caso calcola la Softmax() sugli score forniti
dall'ultimo layer completamente connesso. A titolo di esempio mostriamo anche come sia possibile importare un generico tensore per conoscere
ed eventualmente modificare uno qualsiasi dei tensori all'interno del grafo, per esempio abbiamo considerato l'ultimo livello della parte convoluzionale
della rete.
```python
probs = graph.get_tensor_by_name("import/prob:0")
conv5_3_relu = graph.get_tensor_by_name("import/conv5_3/Relu:0")

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # computing class probabilities
    probss = sess.run(probs)
    probss = np.array(probss).ravel()
    print ("\nPosition = {}".format(np.argmax(probss)))
    probability = probss.max()
writer.close()
```
Viene mostrato il seguente output con la probabilità di classe, nome ed
identificativo di classe:

```

Name = Adam_Goldberg
Probability = 0.9922054409980774

Process finished with exit code 0
```

### Adversarial Example ###
Lo script adv.py genera un'immagine sommando del particolare rumore ad un'altra immagine fornita input, in modo tale da far sbagliare il
classificatore indipendentemente dal fatto che questa immagine sia appartente ad una classe presente nel dataset o meno.
La parte di inizializzazione prevede l'istanza del modello e dei tensori relativi all'immagine, alla predizione e ai relativi score.
Viene definito il grafo come quello di default della sessione, vengono definiti i suoi placeholder e variabili.
C'è da notare come il gradiente venga calcolato sulle features dell'**IMMAGINE** e non sui **PESI**.

```python
model = Vggface()
resized_image = model.resized_image
y_pred = model.y_pred
y_logits = model.y_logits

# Set the graph for the Vggface model as the default graph,
# so that all changes inside this with-block are done to that graph.
with model.graph.as_default():
    # Add a placeholder variable for the target class-number.
    pl_cls_target = tf.placeholder(dtype=tf.int32)

    # Add a new loss-function. This is the cross-entropy.
    # See Tutorial #01 for an explanation of cross-entropy.
    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=y_logits, labels=[pl_cls_target])

    # Get the gradient for the loss-function with regard to
    # the resized input image.
    gradient = tf.gradients(loss, resized_image)

session = tf.Session(graph=model.graph)

```

Questa è la parte più impegnativa dal punto di vista tecnico e teorico, infatti partendo ad alto livello ciò che dobbiamo fare è considerare il metodo adversarial_example():

```python
path = "/PycharmProjects/tensorface/vgg/tf/source.jpg"
adversary_example(image_path=path,
                  cls_target=299,
                  noise_limit=0.09,
                  required_score=0.99)
```

dobbiamo indicare una serie di parametri:

* il path dell'immagine che vogliamo modificare
* la classe target
* il limite di rumore generato ad ogni step
* lo score obiettivo calcolato sull'immagine modificata con il rumore generato

In realtà se seguiamo il flusso d'esecuzione ciò che fa il metodo citato
in precedenza è solo plottare l'immagine, per capire cosa sta succedendo
dobbiamo analizzare il metodo find_adversary_noise().

```python
find_adversary_noise(image_path=image_path,
                             cls_target=cls_target,
                             noise_limit=noise_limit,
                             required_score=required_score)
```
Questo metodo è praticamente identico a quello precedente, questa volta però avremo in output quanto segue:

* image
* noisy_image
* noise
* name_source
* name_target
* score_source
* score_source_org
* score_target

I primi 5 sono abbastanza chiari. Si tratta dell'immagine originale, di
quella con il rumore addizionato, del rumore ed i nomi delle immagini.
Gli ultimi 3 paramtri rappresentano rispettivamente: lo score dell'immagine
con il rumore, di quella originale e di quella della classe di destinazione.


#### Implementazione di "find adversary noise" ####
In questo script l'operazione principale è la run della sessione passando
in ingresso il feed_dict ovvero tutti i parametri che dovranno poi essere
sistemati nei vai placeholder, in questo caso l'immagine da modificare.
In output avremo l'immagine sorgente e le probabilità di classe.

```python
    # Create a feed-dict with the image.
    feed_dict = model._create_feed_dict(image_path=image_path)

    # Use TensorFlow to calculate the predicted class-scores
    # (aka. probabilities) as well as the resized image.
    pred, image = session.run([y_pred, resized_image],
                              feed_dict=feed_dict)
    # Convert to one-dimensional array.
    pred = np.squeeze(pred)

    # Predicted class-number.
    cls_source = np.argmax(pred)
```

Da questo punto in poi si inizia a costruire il rumore, utilizzando i parametri
forniti in ingresso al metodo inizialmente: rumore massimo e score da raggiungere
In modo iterativo viene quindi creato il rumore e addizionato all'immagine,
dopodichè viene calcolato lo score relativo all'immagine ottenuta. Il ciclo
termina se si è raggiunto lo score target oppure se sono state fatte troppe
iterazioni.


### Tensorboard ###
Tool grafico per visualizzare i log ottenuti con tensorflow, è possibile
visualizzare il grafo della rete neurale ed esplorare tutti i suoi nodi.
Aprire il terminale e digitare

```
tensorboard --logdir /path/to/tensorface/vgg/tf/logs

```
Si attiverà un servizio in locale, aprire il browser e digitare

```
localhost:6006
```
l'istruzione presente nello script classify.adv, viene usata per creare un file
di log che contiene il grafo in questo modo è possibile visualizzarlo
con tensorboard

```
writer = tf.summary.FileWriter(logdir=log_path, graph=tf.get_default_graph())
```
A titolo di esempio riportiamo il grafo Vggface esportato con il tool.
Ricordarsi di chiudere il writer per visualizzare il file di log.

```python
writer.close()
```

![VggFace](vgg/tf/graph-vggface.png)


## Contatti ##

* enr.ferrara1_at_studenti_dot_unica_dot_it 
* mar.uras1_at_studenti_dot_unica_dot_it
* ma.pintor1_at_studenti_dot_unica_dot_it